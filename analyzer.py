#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Abre archivo en modo lectura
archivo = open('SCRIPT_CREATE_SP_MERGE.sql','r')

# inicia bucle infinito para leer línea a línea
while True:
    linea = archivo.readline()  # lee línea
    if not linea:
        break  # Si no hay más se rompe bucle
    print(linea)  # Muestra la línea leída
archivo.close  # Cierra archivo
