USE [VWMX_DAM_TRANSFORMATION]
GO
:setvar path "D:\git\cfcn-vwfc-mx\bi-system-implementation\02_Database\02_Transformation\05_Stored_Procedures\"
:r $(path)\SP_MERGE_BORROWER_UNITS_BP.sql
:r $(path)\SP_MERGE_BORROWER_UNITS_RELATIONSHIPS.sql
:r $(path)\SP_DELETE_BORROWER_UNITS_RELATIONSHIPS.sql
:r $(path)\SP_MERGE_BUSINESS_PARTNER.sql
:r $(path)\SP_MERGE_BUSINESS_PARTNER_ADDRESS.sql
